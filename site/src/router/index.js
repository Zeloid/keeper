import Vue from 'vue'
import Router from 'vue-router'
import Charsheet from '@/components/Charsheet'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Charsheet',
      component: Charsheet
    }
  ]
})
