all: info chat

test: chat-test

info:
	@echo "Running as ${USER}"

chat:
	$(MAKE) -C services/chat

chat-test:
	$(MAKE) -C services/chat test